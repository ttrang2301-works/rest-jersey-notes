package ttrang2301.sample.restjersey;

import org.glassfish.jersey.server.ResourceConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestJerseyApplication extends ResourceConfig {

    private static Logger LOGGER = LoggerFactory.getLogger(RestJerseyApplication.class);

    public RestJerseyApplication() {
        String rootPackage = RestJerseyApplication.class.getPackage().getName();
        LOGGER.info("Jersey configuration will recursively scan package: " + rootPackage);
        packages(true, rootPackage);
    }

    public static void main(String[] args) {
        SpringApplication.run(RestJerseyApplication.class, args);
    }

}

