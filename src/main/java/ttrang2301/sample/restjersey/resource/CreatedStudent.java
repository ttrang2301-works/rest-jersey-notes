/**
 * Copyright (c) 2018 Absolute Software Corporation. All rights reserved. Reproduction or
 * transmission in whole or in part, in any form or by any means, electronic, mechanical or
 * otherwise, is prohibited without the prior written consent of the copyright owner.
 */
package ttrang2301.sample.restjersey.resource;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class CreatedStudent extends Student {

    public int id;

    @JsonSerialize(converter = Type.Serializer.class)
    @JsonDeserialize(converter = Type.Deserializer.class)
    public Type type;

    public CreatedStudent(int id, String name, Type type) {
        super(name);
        this.id = id;
        this.type = type;
    }
}
