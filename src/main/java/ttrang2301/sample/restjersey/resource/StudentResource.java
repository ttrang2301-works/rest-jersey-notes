/**
 * Copyright (c) 2018 Absolute Software Corporation. All rights reserved. Reproduction or
 * transmission in whole or in part, in any form or by any means, electronic, mechanical or
 * otherwise, is prohibited without the prior written consent of the copyright owner.
 */
package ttrang2301.sample.restjersey.resource;

import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import ttrang2301.sample.restjersey.resource.exception.NotInSafePlaceException;
import ttrang2301.sample.restjersey.resource.filter.SuccessStatus;

@Path("/data/students")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class StudentResource {

    private static final List<CreatedStudent> createdStudents = new ArrayList<>();

    private static int count = 0;
    private static String[] lastnames = {"Cashie", "Lane", "Kent", "Butler", "O'hara"};

    @POST
    @SuccessStatus(Response.Status.CREATED)
    public CreatedStudent create(Student student) throws NotInSafePlaceException {
        if ((count % (Type.values().length + 1)) == 0) {
            count++;
            throw new NotInSafePlaceException("I don't feel well doing what you ask, ahihi -___-");
        }
        CreatedStudent createdStudent = new CreatedStudent(count,
                student.name + " " + lastnames[count % lastnames.length],
                Type.getInstanceByIndex((count % Type.values().length) + 1));
        createdStudents.add(createdStudent);
        count++;
        return createdStudent;
    }

    @GET
    @Path("/{id}")
    public CreatedStudent getOne(@PathParam("id") int id) {
        Optional<CreatedStudent> foundStudent =
                createdStudents.stream().filter(student -> student.id == id).findFirst();
        if (foundStudent.isPresent()) {
            return foundStudent.get();
        } else {
            throw new NotFoundException("Student with id " + id + " does not exist.");
        }
    }

    @GET
    public List<CreatedStudent> getMultiple(
            @QueryParam("type") String type,
            @QueryParam("name") String name) {
        return createdStudents.stream().filter(student -> {
            if (!StringUtils.isEmpty(name) && !StringUtils.isEmpty(student.name) && !student.name.contains(name)) {
                return false;
            }
            if (type != null && student.type != null && !student.type.getDisplayedText().contains(type)) {
                return false;
            }
            return true;
        }).collect(Collectors.toList());
    }
}
