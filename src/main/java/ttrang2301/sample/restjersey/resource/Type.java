package ttrang2301.sample.restjersey.resource;

import com.fasterxml.jackson.databind.util.StdConverter;

import java.util.Objects;

/**
 * Copyright (c) 2018 Absolute Software Corporation. All rights reserved. Reproduction or
 * transmission in whole or in part, in any form or by any means, electronic, mechanical or
 * otherwise, is prohibited without the prior written consent of the copyright owner.
 */
public enum Type {

    NORMAL("Normal as others", 1), GIFTED("Gifted more than the crowd", 2);

    private String displayedText;
    private int index;

    Type(String displayedText, int index) {
        this.displayedText = displayedText;
        this.index = index;
    }

    public static final Type getInstanceByIndex(int index) {
        for (Type type : Type.values()) {
            if (type.getIndex() == index) {
                return type;
            }
        }
        throw new RuntimeException("Unsupported type at index " + index);
    }

    public String getDisplayedText() {
        return displayedText;
    }

    public int getIndex() {
        return index;
    }

    public static final class Deserializer extends StdConverter<String, Type> {

        @Override
        public Type convert(String value) {
            if (value == null) {
                return null;
            }
            for (Type type : Type.values()) {
                if (Objects.equals(type.getDisplayedText(), value)) {
                    return type;
                }
            }
            throw new RuntimeException("Unsupported type: '" + value + "'");
        }

    }

    public static final class Serializer extends StdConverter<Type, String> {

        @Override
        public String convert(Type type) {
            if (type == null) {
                return null;
            }
            return type.getDisplayedText();
        }

    }
}
