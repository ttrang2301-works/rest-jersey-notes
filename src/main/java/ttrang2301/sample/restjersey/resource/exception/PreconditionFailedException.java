/**
 * Copyright (c) 2018 Absolute Software Corporation. All rights reserved. Reproduction or
 * transmission in whole or in part, in any form or by any means, electronic, mechanical or
 * otherwise, is prohibited without the prior written consent of the copyright owner.
 */
package ttrang2301.sample.restjersey.resource.exception;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.core.Response;

public class PreconditionFailedException extends ClientErrorException {

    public PreconditionFailedException(String message) {
        super(message, Response.Status.PRECONDITION_FAILED);
    }

}
