/**
 * Copyright (c) 2018 Absolute Software Corporation. All rights reserved. Reproduction or
 * transmission in whole or in part, in any form or by any means, electronic, mechanical or
 * otherwise, is prohibited without the prior written consent of the copyright owner.
 */
package ttrang2301.sample.restjersey.resource;

public class Student {

    public String name;

    public Student() {
    }

    public Student(String name) {
        this.name = name;
    }
}
