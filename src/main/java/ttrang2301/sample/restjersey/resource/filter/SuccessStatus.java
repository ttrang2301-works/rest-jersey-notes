package ttrang2301.sample.restjersey.resource.filter;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.ws.rs.NameBinding;
import javax.ws.rs.core.Response;

/**
 * Copyright (c) 2018 Absolute Software Corporation. All rights reserved. Reproduction or
 * transmission in whole or in part, in any form or by any means, electronic, mechanical or
 * otherwise, is prohibited without the prior written consent of the copyright owner.
 */

@NameBinding
@Retention(RetentionPolicy.RUNTIME)
public @interface SuccessStatus {

    Response.Status value();

}
